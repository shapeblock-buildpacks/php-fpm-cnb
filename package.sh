GOOS=linux go build -ldflags="-s -w" -o ./bin/build ./cmd/build/main.go
GOOS=linux go build -ldflags="-s -w" -o ./bin/detect ./cmd/detect/main.go
pack buildpack package registry.gitlab.com/shapeblock-buildpacks/php-fpm-cnb --config ./package.toml --publish