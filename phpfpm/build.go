package phpfpm

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

func Build() packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {
		phpFpmLayer, err := context.Layers.Get("php-fpm")
		if err != nil {
			return packit.BuildResult{}, err
		}

		binPath := strings.Join([]string{os.Getenv("PATH"), filepath.Join(os.Getenv("PHP_HOME"), "bin")}, string(os.PathListSeparator))
		err = os.Setenv("PATH", binPath)
		if err != nil {
			return packit.BuildResult{}, err
		}

		matches, err := filepath.Glob(os.Getenv("PHP_HOME") + "/lib/php/extensions/no-debug-non-zts-*")
		if err != nil {
			return packit.BuildResult{}, err
		}
		// phpfpm.php.ini
		phpIniConfig := PhpIniConfig{
			PhpHome:      os.Getenv("PHP_HOME"),
			ExtensionDir: matches[0],
			Extensions: []string{
				"bz2",
				"curl",
				"dba",
				"enchant",
				"fileinfo",
				"gd",
				"ldap",
				"mbstring",
				"openssl",
				"pdo",
				"pdo_mysql",
				"pdo_sqlite",
				"readline",
				"soap",
				"sodium",
				"sockets",
				"xsl",
				"zip",
				"zlib",
			},
			ZendExtensions: []string{
				"xdebug",
			},
		}

		fpmPhpIni := filepath.Join(phpFpmLayer.Path, "php.ini")
		err = ProcessTemplateToFile(PhpIniTemplate, fpmPhpIni, phpIniConfig)
		if err != nil {
			return packit.BuildResult{}, err
		}

		phpFpmConfig := PhpFpmConfig{
			FpmDir: phpFpmLayer.Path,
		}
		fpmConf := filepath.Join(phpFpmLayer.Path, "php-fpm.conf")
		err = ProcessTemplateToFile(PhpFpmConfTemplate, fpmConf, phpFpmConfig)
		if err != nil {
			return packit.BuildResult{}, err
		}

		phpFpmLayer.Build = true
		phpFpmLayer.Launch = true
		phpFpmLayer.Cache = true

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.BuildResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.BuildResult{}, err
		}

		startCmd := fmt.Sprintf("php-fpm -c %s -y %s/php-fpm.conf", phpFpmLayer.Path, phpFpmLayer.Path)
		if config.Web.Commands.Start != "" {
			startCmd = config.Web.Commands.Start
		}

		fmt.Printf("Run command -> %s\n", startCmd)

		phpFpmLayer.SharedEnv.Append("PATH", filepath.Join(os.Getenv("PHP_HOME"), "bin"), string(os.PathListSeparator))
		phpFpmLayer.SharedEnv.Append("PATH", filepath.Join(os.Getenv("PHP_HOME"), "sbin"), string(os.PathListSeparator))

		phpFpmLayer.SharedEnv.Default("PHP_FPM_DIR", phpFpmLayer.Path)
		phpFpmLayer.LaunchEnv.Default("LD_LIBRARY_PATH", filepath.Join(os.Getenv("PHP_HOME"), "lib"))
		phpFpmLayer.LaunchEnv.Default("PHPRC", phpFpmLayer.Path)
		return packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				phpFpmLayer,
			},
			Launch: packit.LaunchMetadata{
				Processes: []packit.Process{
					{
						Type:    "web",
						Command: startCmd,
					},
				},
			},
		}, nil
	}
}
