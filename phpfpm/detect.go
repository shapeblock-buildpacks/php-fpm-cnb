package phpfpm

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

type PlatformConfig struct {
	Name  string `yaml:"name"`
	Type  string `yaml:"type"`
	Build struct {
		Flavor string `yaml:"flavor"`
	} `yaml:"build,omitempty"`
	Web struct {
		Commands struct {
			Start string `yaml:"start"`
		} `yaml:"commands"`
	} `yaml:"web"`
	X map[string]interface{} `yaml:"-"`
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Detect() packit.DetectFunc {
	return func(context packit.DetectContext) (packit.DetectResult, error) {

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.DetectResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.DetectResult{}, err
		}

		// Dont' detect if stack is not php
		res := strings.Split(config.Type, ":")
		stack, _ := res[0], res[1]

		if stack != "php" {
			return packit.DetectResult{}, err
		}

		return packit.DetectResult{
			Plan: packit.BuildPlan{
				Provides: []packit.BuildPlanProvision{
					{Name: "php-fpm"},
				},
				Requires: []packit.BuildPlanRequirement{
					{
						Name: "php-fpm",
					},
				},
			},
		}, nil
	}
}
