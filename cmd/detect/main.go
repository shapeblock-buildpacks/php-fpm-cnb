package main

import (
	"gitlab.com/shapeblock-buildpacks/php-fpm-cnb/phpfpm"

	"github.com/paketo-buildpacks/packit"
)

func main() {
	packit.Detect(phpfpm.Detect())
}
