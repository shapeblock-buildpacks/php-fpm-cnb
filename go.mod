module gitlab.com/shapeblock-buildpacks/php-fpm-cnb

go 1.16

require (
	github.com/google/go-containerregistry v0.5.1 // indirect
	github.com/paketo-buildpacks/packit v1.3.1 // indirect
	github.com/spf13/cobra v1.1.3 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
